db.course_bookings.insertMany([
{ "courseId": "C001", "studentID": "S004", "isCompleted": true},
{ "courseId": "C002", "studentID": "S001", "isCompleted": false},
{ "courseId": "C001", "studentID": "S003", "isCompleted": true},
{ "courseId": "C003", "studentID": "S002", "isCompleted": false},
{ "courseId": "C001", "studentID": "S002", "isCompleted": true},
{ "courseId": "C004", "studentID": "S003", "isCompleted": false},
{ "courseId": "C002", "studentID": "S004", "isCompleted": true},
{ "courseId": "C003", "studentID": "S007", "isCompleted": false},
{ "courseId": "C001", "studentID": "S005", "isCompleted": true},
{ "courseId": "C004", "studentID": "S008", "isCompleted": false},
{ "courseId": "C001", "studentID": "S013", "isCompleted": true},
]);